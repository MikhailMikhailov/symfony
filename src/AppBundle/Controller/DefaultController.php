<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
//use Symfony\Component\BrowserKit\Response;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
       // return $this->render('default/index.html.twig', [
       //     'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
       // ]);
                // replace this example code with whatever you need
     //   return $this->render('default/index.html.twig', [
     //       'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
     //   ]);
        $user = $this->getUser();
        $username = "Anonimous";
        if($user){
            $username = $user->getUsername();
        }
        return $this->render(
            'default/index.html.twig',
            array('username' => $username)
        );
    }
}
